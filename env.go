package env

import (
	"bufio"
	"os"
	"strings"
)

type EnvConfig struct {
	value map[string]string
}

func (ec *EnvConfig) Load(filename string) error {

	if filename == "" {
		filename = ".env"
	}

	file, err := os.Open(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	ec.value = make(map[string]string)

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		line := scanner.Text()

		parts := strings.SplitN(line, "=", 2)

		if len(parts) == 2 {
			key := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])
			ec.value[key] = value
		}
	}

	if err := scanner.Err(); err != nil {
		return err
	}

	return nil
}

func (ec *EnvConfig) Get(key string) string {
	return ec.value[key]
}
